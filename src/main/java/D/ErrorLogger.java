package D;

import util.DatabaseConnection;
import util.FileWriter;

public class ErrorLogger extends Logger{

    private DatabaseConnection logDbc;
    private String logFileLocation;

    @Override
    public void log(String error)
    {
        logDbc.LogError("An error occured: ", error);
        FileWriter.write(logFileLocation, error);
    }
}
