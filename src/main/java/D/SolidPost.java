package D;

import util.DatabaseConnection;

/*
    Dependency inversion principle

    1. High-level modules should not depend on low-level modules. Both should depend on abstractions.
    2. Abstractions should not depend on details. Details should depend on abstractions.

    To comply with this exact principle we are using the "dependency inversion pattern" most often
    solved with (and known as) "dependency injection".

    SolidPost is not depending on ErrorLogger class specifically or instantiating it directly, rather
    it takes an abstract Logger class that can be instantiated (injected) with ErrorLogger class or
    any other class that extends that abstraction.

    Solid post can be injected with a lThe logger is "injected" with the creation of SolidPost instance:
                SolidPost complexPoster = new SolidPost(new ErrorLogger());
                SolidPost simplePoster = new SolidPost(new SimpleLogger());
 */

public class SolidPost {

    private Logger logger;

    public SolidPost(Logger injectedLogger) {
        logger = injectedLogger;
    }

    void createPost(DatabaseConnection dbc, String postMessage) {
        try {
            dbc.post(postMessage);
        } catch (Exception ex) {
            logger.log(ex.toString());
        }
    }

}

/*
 */