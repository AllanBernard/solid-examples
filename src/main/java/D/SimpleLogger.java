package D;

public class SimpleLogger extends Logger {

    @Override
    public void log(String message) {
        System.out.println("Just a logger logging a log: " + message);
    }

}
