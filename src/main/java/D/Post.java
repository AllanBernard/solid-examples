package D;

import util.DatabaseConnection;

/*
    Dependency inversion principle

    1. High-level modules should not depend on low-level modules. Both should depend on abstractions.
    2. Abstractions should not depend on details. Details should depend on abstractions.

 */

public class Post {

    private ErrorLogger errorLogger = new ErrorLogger();

    void createPost(DatabaseConnection dbc, String postMessage) {
        try {
            dbc.post(postMessage);
        } catch (Exception ex) {
            errorLogger.log(ex.toString());
        }
    }

}