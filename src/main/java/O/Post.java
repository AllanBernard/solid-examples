package O;

import util.DatabaseConnection;

/*
    Open/closed principle

    Classes, modules and functions should be open for extension but closed for modification.
 */

public class Post {

    DatabaseConnection dbc;

    void createPost(String postMessage) {
        if(postMessage.startsWith("#")) {
            dbc.addAsTags(postMessage);
        } else {
            dbc.post(postMessage);
        }
    }

}