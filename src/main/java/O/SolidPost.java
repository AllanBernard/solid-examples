package O;

import util.DatabaseConnection;

/*
    Open/closed principle

    Classes, modules and functions should be open for extension but closed for modification.

    You should not cram different implementations of the same functionality into a single element.

    Now the implementations of different functions can be handled or changed independently of each other
    Posting a regular post and posting as tags, based on the first letter should be checked elsewhere,
    at a higher level.

    Remember - you can always substitute a parent instance with it's sibling instance!
    Everywhere you use SolidPost class you can use SolidPostWithTags as well ((but not vice-versa)).

    Note that you can still easily change the underlying base functionality (like the database connection)
    without affecting or having to modify any of the extended pieces. If you change not overridden content
    of the parent class, all it's children inherit the change automatically.
 */

class SolidPost {

    DatabaseConnection dbc;

    void CreatePost(String postMessage) {
        dbc.post(postMessage);
    }

}

class SolidPostWithTags extends SolidPost {

    @Override
    void CreatePost(String postMessage) {
        dbc.addAsTags(postMessage);
    }

}
