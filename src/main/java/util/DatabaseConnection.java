package util;

import java.util.ArrayList;
import java.util.List;

public class DatabaseConnection {

    public void saveComment(String comment) {
        System.out.println("Comment saved");
    }

    public void LogError(String errorDescription, String errorMessage) {
        System.out.println("Error logged to database");
        System.out.println(errorDescription);
        System.out.println(errorMessage);
    }

    public void post(String post) {
        System.out.println("Post added");
        System.out.println(post);
    }

    public void addAsTags(String post) {
        System.out.println("Post added as a tag");
        System.out.println(post);
    }

    public void notifyUser(String user){
        System.out.println("User notified");
        System.out.println(user);
    }

    public void overrideExistingMention(String user, String postMessage) {
        System.out.println("Existing mention overriden");
    }

    public List<String> getUnhandledPostsMessages() {
        return new ArrayList<>();
    }
}
