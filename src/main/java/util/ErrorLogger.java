package util;

public class ErrorLogger {

    private static final ErrorLogger errorLoggerInstance = new ErrorLogger();
    private DatabaseConnection logDbc;
    private String logFileLocation;

    private ErrorLogger() {}

    public static final ErrorLogger getInstance() {
        return errorLoggerInstance;
    }

    public void log(String error)
    {
        logDbc.LogError("An error occured: ", error);
        FileWriter.write(logFileLocation, error);
    }
}
