package I;

/*
    Interface segregation principle

    No client should be forced to depend on methods it does not use

 */

interface Post {
    void createPost();
}

interface PostNew {
    void createPost();
    void readPost();
}

/*
    - I first create an interface of Post that has the createPost() method and implement it.
    - Now later on I need a different implementation that needs readPost() so I go and create another
      interface declaring both methods and using that for both classes. Or worse - update first interface.

    It works but I am violating the interface segregation principle.

    Lets rewrite that...
 */

interface SolidPostCreate {
    void createPost();
}

interface SolidPostRead {
    void readPost();
}

/*
    Now all classes needing either the createPost() or readPost() methods, can implement those separately.
    Classes needing both just implement both interfaces.

    Remember! ALL abstract methods MUST be implemented by the FIRST concrete class that uses them.

    When doing it wrong you are supplying "a banana to a client who wants a banana...
            ...together with the gorilla and rain forest"

    Multiple interface methods are if fine all implementations of that interface actually use them

    interface Car {
        void startEngine();
        void startDriving();
        void stopDriving()
        void stopEngine();
    }

    class SportsCar implements Car
    class Buggy implements Car
    class ElectricCar implements Car
    class Truck implements Car
 */