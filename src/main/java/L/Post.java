package L;

import util.DatabaseConnection;
import util.User;

import java.util.List;

/*

    Liskov substitution  principle

    1. if 'S' is a subtype of 'T'
    2. then objects of type 'T' may be replaced (substituted) with objects of 'S'

        or more accurately:
            Let ϕ(x) be a property provable about objects x of type T.
            Then ϕ(y) should be true for objects y of type S, where S is a subtype of T

    "If it looks like a duck and quacks like a duck but needs batteries... you'll need a better analogy"
 */

class Post {
    DatabaseConnection dbc;

    void createPost(String postMessage) {
        dbc.post(postMessage);
    }
}

class TagPost extends Post {
    @Override
    void createPost(String postMessage) {
        dbc.addAsTags(postMessage);
    }
}

class MentionPost extends Post {

    void createMentionPost(String postMessage) {
        String user = User.parseUser(postMessage);

        dbc.notifyUser(user);
        dbc.overrideExistingMention(user, postMessage);
        dbc.post(postMessage);
    }
}


class PostHandler {

    void handleNewPosts() {
        DatabaseConnection database = new DatabaseConnection();
        List<String> newPosts = database.getUnhandledPostsMessages();

        for (String postMessage : newPosts) {
            Post post;

            if (postMessage.startsWith("#")) {
                post = new TagPost();
            } else if (postMessage.startsWith("@")) {
                post = new MentionPost();
            } else {
                post = new Post();
            }

            post.createPost(postMessage);   //!!!
        }
    }
}


/*
    !!!
    Observe how the call of createPost() in the case of a subtype MentionPost would'nt do what it is supposed to:
    notify the user and override existing mention.

    Since the createPost() method is not overridden in MentionPost the createPost() call would simply be delegated
    upwards in the class hierarchy and call createPost() from its parent class.
 */
