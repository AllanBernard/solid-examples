package L;

import util.DatabaseConnection;
import util.User;

import java.util.List;

/*

    Liskov substitution  principle

    "If it looks like a duck and quacks like a duck but needs batteries... you'll need a better analogy"

    1. if 'S' is a subtype of 'T'
    2. then objects of type 'T' may be replaced (substituted) with objects of 'S'

        or more accurately:
            Let ϕ(x) be a property provable about objects x of type T.
            Then ϕ(y) should be true for objects y of type S, where S is a subtype of T

    (For brevity multiple classes are located in this file, should be separate files)
 */

class SolidPost {
    DatabaseConnection dbc;

    void createPost(String postMessage) {
        dbc.post(postMessage);
    }
}

class SolidTagPost extends SolidPost {
    @Override
    void createPost(String postMessage) {
        dbc.addAsTags(postMessage);
    }
}

class SolidMentionPost extends SolidPost {

    @Override
    void createPost(String postMessage) {
        String user = User.parseUser(postMessage);
        notifyUser(user);
        overrideExistingMention(user, postMessage);
        dbc.post(postMessage);
    }

    private void notifyUser(String user) {
        dbc.notifyUser(user);
    }

    private void overrideExistingMention(String user, String postMessage) {
        dbc.overrideExistingMention(user, postMessage);
    }

}

class SolidPostHandler {

    void handleNewPosts() {
        DatabaseConnection database = new DatabaseConnection();
        List<String> newPosts = database.getUnhandledPostsMessages();

        for (String postMessage : newPosts) {
            SolidPost solidPost;

            if (postMessage.startsWith("#")) {
                solidPost = new SolidTagPost();
            } else if (postMessage.startsWith("@")) {
                solidPost = new SolidMentionPost();
            } else {
                solidPost = new SolidPost();
            }

            solidPost.createPost(postMessage);
        }
    }
}


/*
    By overriding createPost() method rather than calling it on its base we no longer violate the Liskov substitution
    principle. The parent class SolidPost can be freely substituted with its child class SolidMentionPost, without
    knowing or specifically looking into the inner workings of the child class.

    This is just a very simple example. Liskov substitution principle is a hard concept to wrap your head around,
    it can manifest in a variety of ways and violations are not always easy to identify.
 */
