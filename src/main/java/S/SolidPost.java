package S;

import util.DatabaseConnection;
import util.ErrorLogger;

/*
    Single responsibility principle

    Every module or class should have responsibility over a single part of the programmed functionality.

    A class or module should have one and only one reason to be changed!

    PostComment had too much responsibility - it had to create a new post but also log an error.
    PostComment should not know or care that an error has to be logged both to DB and file and exactly how to do it.
    That functionality had to be moved to a separate class.

 */

public class SolidPost {

    private ErrorLogger errorLogger = ErrorLogger.getInstance();

    void PostComment(DatabaseConnection dbc, String postComment) {
        try {
            dbc.saveComment(postComment);
        } catch (Exception ex) {
            errorLogger.log(ex.toString());
        }
    }

}
