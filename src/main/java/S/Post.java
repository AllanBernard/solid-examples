package S;

import util.DatabaseConnection;
import util.FileWriter;

/*
    Single responsibility principle

    Every module or class should have responsibility over a single part of the programmed  functionality.

 */

public class Post {

    void PostComment(DatabaseConnection dbl, String postComment) {
        try {
            dbl.saveComment(postComment);
        } catch (Exception ex) {
            dbl.LogError("An error occurred: ", ex.toString());
            FileWriter.write("LocalErrors.txt", ex.toString());
        }
    }

}
